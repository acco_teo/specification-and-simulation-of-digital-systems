----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    13:11:48 11/25/2018
-- Design Name:
-- Module Name:    fsm_2 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_2 is
	port (up_dw: in std_logic;
				clk : in  std_logic;
				rst : in  std_logic;
				cnt : out std_logic_vector(3 downto 0));
end fsm_2;

architecture behavioral of fsm_2 is
	signal curr_state, next_state: std_logic_vector(3 downto 0);
begin

	StateReg: process(clk, rst)
	begin
		if(rst = '1') then
			curr_state <= "0001";
		elsif(clk'event and clk = '1') then
			curr_state <= next_state;
		end if;
	end process;

	CombLogic: process(curr_state, up_dw)
	begin
		if(up_dw = '0') then
			next_state <= curr_state(2 downto 0) & curr_state(3);
		else
			next_state <= curr_state(0) & curr_state(3 downto 1);
		end if;
	end process;

	cnt <= curr_state;

end behavioral;
