----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    17:50:33 11/21/2018
-- Design Name:
-- Module Name:    fsm_1 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_1 is
	port (P : in  std_logic;
				R : out std_logic;
				clk : in  std_logic;
				rst : in  std_logic);
end fsm_1;

architecture beh_2proc of fsm_1 is
	type state_type is (A, B, C, D);
	signal curr_state, next_state: state_type;

begin

	StateReg: process(clk, rst)
	begin
		if(rst = '1') then
			curr_state <= A;
		elsif(clk'event and clk = '1') then
			curr_state <= next_state;
		end if;
	end process;

	CombLogic: process(curr_state, P)
	begin
		case curr_state is
			when A =>
						R <= '0';
						if(P = '1') then
							next_state <= B;
						else
							next_state <= A;
						end if;
			when B =>
						R <= '0';
						if(P = '1') then
							next_state <= C;
						else
							next_state <= B;
						end if;
			when C =>
						R <= '0';
						if(P = '1') then
							next_state <= D;
						else
							next_state <= C;
						end if;
			when D =>
						R <= '1';
						if(P = '1') then
							next_state <= B;
						else
							next_state <= A;
						end if;
			when others => R <= 'Z';
										 next_state <= A;
		end case;
	end process;

end beh_2proc;
