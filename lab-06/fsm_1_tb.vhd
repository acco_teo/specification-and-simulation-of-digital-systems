--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   19:24:32 11/21/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-06/fsm_1/fsm_1_tb.vhd
-- Project Name:  fsm_1
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: fsm_1
--
-- Dependencies: test_vectors_fsm_1.txt
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use STD.TEXTIO.ALL;
USE ieee.numeric_std.ALL;

entity fsm_1_tb is
end fsm_1_tb;

architecture behavior of fsm_1_tb is

    -- Component Declaration for the Unit Under Test (UUT)
    component fsm_1
      port (P : in  std_logic;
            R : out  std_logic;
            clk : in  std_logic;
            rst : in  std_logic);
    end component;


   -- Inputs
   signal P_s : std_logic := '0';
   signal clk_s : std_logic := '0';
   signal rst_s : std_logic := '0';

 	-- Output
   signal R_s : std_logic;

	-- Delay
	constant delay : time := 5 ns;

   -- Clock period definitions
   constant clk_period : time := 10 ns;

	file file_vectors : text;

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: entity work.fsm_1(beh_2proc) port map (
          P => P_s,
          R => R_s,
          clk => clk_s,
          rst => rst_s
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;

-- text-vectors.txt format
-- P			reset			R
-- txt_P	txt_rst		txt_R
-- ______________________
--  1 			1				A
--  1 			0				B
--	0 			0				B
--	1 			0				C
--	1 			0				D
--	0 			0				A
--	1 			0				B
--	1 			0				C
--	0 			0				C
--	0 			0				C
--	1 			0				D
--	1 			0				B
--	0 			0				B
--	0 			0				B
--	1 			0				C
--	0 			0				C
--	1 			0				D
--	0 			0				A

  process
		variable v_iline : line;
		variable txt_P: integer;
		variable txt_rst: integer;
		variable txt_R: integer;

		function to_std_logic(i : in integer) return std_logic is
		begin
			if i = 0 then
				return '0';
			end if;
			return '1';
		end function;

	begin
		file_open(file_vectors, "test_vectors.txt", read_mode);

		while not endfile(file_vectors) loop
			readline(file_vectors, v_iline);
			read(v_iline, txt_P);
			read(v_iline, txt_rst);
			read(v_iline, txt_R);
			P_s <= to_std_logic(txt_P);
			rst_s <= to_std_logic(txt_rst);
			wait for clk_period;
			assert R_s = to_std_logic(txt_R);
		end loop;

		file_close(file_vectors);
	end process;

end;
