----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    14:34:21 11/20/2018
-- Design Name:
-- Module Name:    de_brujin_counter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity de_brujin_counter is
	port (rst: in  std_logic;
				clk: in  std_logic;
				db_counter: out std_logic_vector(7 downto 0));
end de_brujin_counter;

architecture behavioral of de_brujin_counter is
	signal currstate, nextstate: std_logic_vector(7 downto 0);

begin

	StateReg: process(clk, rst)
	begin
		if(rst = '1') then
			currstate <= (7 => '1', others =>'0');
		elsif(clk = '1' and clk'event) then
			currstate <= nextstate;
      end if;
	end process;

	CombLogic: process(currstate)
	begin
		if(currstate = "00000001") then
			nextstate <= (others => '0');
		elsif(currstate = "00000000") then
			nextstate <= (7 => '1', others => '0');
		else
			nextstate <= (currstate(0) xor currstate(1)) & currstate(7 downto 1);
		end if;
	end process;

	db_counter <= nextstate;

end behavioral;
