----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    13:50:26 11/20/2018
-- Design Name:
-- Module Name:    linear_feedback_shift_register - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity linear_feedback_shift_register is
	port (rst : in  std_logic;
				clk : in  std_logic;
				lfsr: out  std_logic_vector(7 downto 0));
end linear_feedback_shift_register;

architecture behavioral of linear_feedback_shift_register is
	signal currstate, nextstate: std_logic_vector (7 downto 0);
begin

	StateReg: process(clk, rst)
	begin
		if(rst = '1') then
			currstate <= (7 => '1', others =>'0');
		elsif(clk = '1' and clk'event) then
			currstate <= nextstate;
      end if;
	end process;

	nextstate <= (currstate(0) xor currstate(1)) & currstate(7 DOWNTO 1);
	lfsr <= currstate;

end behavioral;
