--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   17:55:44 11/25/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-06/fsm_2/fsm_2_tb.vhd
-- Project Name:  fsm_2
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: fsm_2
--
-- Dependencies: test_vectors_fsm_2.txt
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.TEXTIO.ALL;
use IEEE.STD_LOGIC_TEXTIO.all;

entity fsm_2_tb is
end fsm_2_tb;

architecture behavior of fsm_2_tb is

    -- Component Declaration for the Unit Under Test (UUT)

    component fsm_2
      port (up_dw: in  std_logic;
            clk: IN  std_logic;
            rst: IN  std_logic;
            cnt : OUT  std_logic_vector(3 downto 0));
    end component;


   --Inputs
   signal up_dw_s : std_logic := '0';
   signal clk_s : std_logic := '0';
   signal rst_s : std_logic := '0';

 	--Outputs
   signal cnt_s : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;

	file file_vectors : text; -- format: <up_dw> <rst> <cnt>

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: fsm_2 port map (
          up_dw => up_dw_s,
          clk => clk_s,
          rst => rst_s,
          cnt => cnt_s
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;


   -- Stimulus process
   stim_proc: process
		variable v_iline : line;
		variable txt_rst : integer;
		variable txt_up_dw : integer;
		variable txt_cnt : std_logic_vector(3 downto 0);

		function to_std_logic(i : in integer) return std_logic is
		begin
			if i = 0 then
				return '0';
			end if;
			return '1';
		end function;

	begin
		file_open(file_vectors, "test_vectors_fsm_2.txt", read_mode);

		while not endfile(file_vectors) loop
			readline(file_vectors, v_iline);
			read(v_iline, txt_up_dw);
			read(v_iline, txt_rst);
			read(v_iline, txt_cnt);
			up_dw_s <= to_std_logic(txt_up_dw);
			rst_s <= to_std_logic(txt_rst);
			wait for clk_period;
			assert cnt_s = txt_cnt;
		end loop;
      wait;
   end process;

end;
