----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date: 17:27:53 11/14/2018
-- Design Name:
-- Module Name: d3_decimal_counter - structural
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description: three digits (12 bits) decimal counter
--
-- Dependencies: decimal_counter.vhd
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity d3_decimal_counter is
	port (rst : 	in  std_logic;
				clk : 	in  std_logic;
				enbl: 	in  std_logic;
				trm : 	out std_logic;
				d_count: out std_logic_vector(11 downto 0));
end d3_decimal_counter;

architecture struct of d3_decimal_counter is

	component decimal_counter is
		port (reset 		: in  std_logic;
					clock 		: in  std_logic;
					enable 		: in  std_logic;
					next_digit: out std_logic;
					d_count 	: out std_logic_vector(3 downto 0));
	end component;

	signal n0, n1: std_logic;

begin

	dec_cnt0: decimal_counter port map(reset => rst,
													clock => clk,
													enable => enbl,
													next_digit => n0,
													d_count => d_count(3 downto 0));

	dec_cnt1: decimal_counter port map(reset => rst,
													clock => n0,
													enable => enbl,
													next_digit => n1,
													d_count => d_count(7 downto 4));

	dec_cnt2: decimal_counter port map(reset => rst,
													clock => n1,
													enable => enbl,
													next_digit => trm,
													d_count => d_count(11 downto 8));

end struct;
