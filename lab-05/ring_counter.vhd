----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    12:30:53 11/14/2018
-- Design Name:
-- Module Name:    ring_counter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ring_counter is
	generic (N: integer:= 4);
	port (clk 		: in  std_logic;
				rst 		: in  std_logic;
				r_cnt : out  std_logic_vector(N-1 downto 0));
end ring_counter;

architecture behavioral of ring_counter is
	signal seed: std_logic_vector (N-1 downto 0);
begin

	StateReg: process(clk)
	begin
		if(clk = '1' and clk'event) then
			if(rst = '1') then
				seed <= (0 => '1', others => '0');
			else
				seed <= seed(N-2 downto 0) & seed(N-1);
			end if;
		end if;
	end process;

	OutputProcess: process(seed)
	begin
		r_cnt <= seed;
	end process;
end behavioral;
