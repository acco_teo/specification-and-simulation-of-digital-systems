--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   17:57:59 11/14/2018
-- Design Name:
-- Module Name:
-- Project Name:  d3_decimal_counter
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: d3_decimal_counter
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity d3_decimal_counter_tb is
end d3_decimal_counter_tb;

architecture behavior of d3_decimal_counter_tb is

    -- Component Declaration for the Unit Under Test (UUT)
    component d3_decimal_counter
		port (rst : 	in  std_logic;
				clk : 	in  std_logic;
				enbl: 	in  std_logic;
				trm : 	out std_logic;
				d_count: out  std_logic_vector(11 downto 0));
    end component;


   --Inputs
   signal rst_s : std_logic := '0';
   signal clk_s : std_logic := '0';
   signal enbl_s : std_logic := '0';

 	--Outputs
   signal trm_s : std_logic;
   signal d_count_s : std_logic_vector(11 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;

BEGIN

	-- Instantiate the Unit Under Test (UUT)
   uut: d3_decimal_counter port map(
          rst => rst_s,
          clk => clk_s,
          enbl => enbl_s,
          trm => trm_s,
			 next_digit0 => next_digit0_s,
          next_digit1 => next_digit1_s,
			 d_count => d_count_s);

   -- Clock process definitions
   clk_process :process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin
		enbl_s <= '0';
		rst_s <= '0';
		wait for 2*clk_period;
		enbl_s <= '1';
		wait for 2*clk_period;
		rst_s <= '1';
		wait for 2*clk_period;
		rst_s <= '0';
      wait;
   end process;

END;
