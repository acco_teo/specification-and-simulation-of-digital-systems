----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    16:31:29 11/14/2018
-- Design Name:
-- Module Name:    decimal_counter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity decimal_counter is
	port (reset 		: in  std_logic;
				clock 		: in  std_logic;
				enable 		: in  std_logic;
				next_digit: out std_logic;
				d_count 	: out std_logic_vector(3 downto 0));
end decimal_counter;

architecture behavioral of decimal_counter is
	signal q: unsigned(3 downto 0);

begin

	StateReg: process(clock, reset)
	begin
		if(reset = '1') then 											-- reset event
			q <= "0000";
			next_digit <= '0';
		elsif(clock = '1' and clock'event) then		-- clock event
			if(q = "1001") then
				q <= "0000";
				next_digit <= '1';
			else
				q <= q + 1;
				next_digit <= '0';
			end if;
		end if;
	end process;

	OutputProcess: process(enable, q)
	begin
		if(enable = '1') then 								-- counter enabled
			d_count <= std_logic_vector(q);
		else																	-- counter disabled
			d_count <= "ZZZZ";
		end if;
	end process;

end behavioral;
