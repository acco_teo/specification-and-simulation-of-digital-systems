--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   12:49:17 11/14/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-05/ring_counter/ring_counter_tb.vhd
-- Project Name:  ring_counter
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: ring_counter
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ring_counter_tb is
end ring_counter_tb;

architecture behavior of ring_counter_tb is

    -- Component Declaration for the Unit Under Test (UUT)
	 component ring_counter
		port (clk 	: in  std_logic;
          rst 	: in  std_logic;
          r_cnt : out  std_logic_vector(3 downto 0));
    end component;

   --Inputs
   signal clk_s : std_logic := '0';
   signal rst_s : std_logic := '0';

 	--Outputs
   signal r_cnt_s : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: ring_counter port map (clk => clk_s, rst => rst_s, r_cnt => r_cnt_s);

   -- Clock process definitions
   clk_process :process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin
		rst_s <= '1';
		wait for clk_period;
		rst_s <= '0';
		wait for 5*clk_period;
		rst_s <= '1';
		wait for clk_period;
		rst_s <= '0';
    wait;
   end process;

end;
