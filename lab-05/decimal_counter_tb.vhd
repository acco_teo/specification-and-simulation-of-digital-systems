ss--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   16:59:09 11/14/2018
-- Design Name:
-- Module Name:
-- Project Name:  decimal_counter
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: decimal_counter
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity decimal_counter_tb is
end decimal_counter_tb;

architecture behavior of decimal_counter_tb is

    -- Component Declaration for the Unit Under Test (UUT)
    component decimal_counter
		port (reset 		: in  std_logic;
          clock 		: in  std_logic;
          enable 		: in  std_logic;
          next_digit: out std_logic;
          d_count 	: out std_logic_vector(3 downto 0));
    end component;

   --Inputs
   signal reset_s : std_logic := '0';
   signal clock_s : std_logic := '0';
   signal enable_s : std_logic := '0';

 	--Outputs
   signal next_digit_s : std_logic;
   signal d_count_s : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clock_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: decimal_counter port map (
          reset => reset_s,
          clock => clock_s,
          enable => enable_s,
          next_digit => next_digit_s,
          d_count => d_count_S
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock_s <= '0';
		wait for clock_period/2;
		clock_s <= '1';
		wait for clock_period/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin
		enable_s <= '0';
		reset_s <= '0';
		wait for 2*clock_period;
		enable_s <= '1';
		wait for 2*clock_period;
		reset_s <= '1';
		wait for 2*clock_period;
		reset_s <= '0';
    wait;
   end process;

END;
