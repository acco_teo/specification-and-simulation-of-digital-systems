--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   14:20:11 11/13/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-04/gray_counter/gray_counter_tb.vhd
-- Project Name:  gray_counter
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: gray_counter
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity gray_counter_tb is
end gray_counter_tb;

architecture behavior of gray_counter_tb is

    -- Component Declaration for the Unit Under Test (UUT)
    component gray_counter
		port (reset : in  std_logic;
          clk 	: in  std_logic;
          g_cnt : out  std_logic_vector(3 downto 0));
    end component;


   --Inputs
   signal reset_s : std_logic := '0';
   signal clk_s 	: std_logic := '0';

 	--Outputs
   signal g_cnt_s : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: gray_counter port map (reset => reset_s, clk => clk_s, g_cnt => g_cnt_s);

   -- Clock process definitions
   clk_process: process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;

	-- Stimulus process
   stim_proc: process
   begin
		reset_s <= '1';
		wait for clk_period;
		reset_s <= '0';
    wait;
   end process;

end;
