----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    13:42:35 11/13/2018
-- Design Name:
-- Module Name:    gray_counter - behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gray_counter is
	generic (N: integer := 4);
	port (reset : in  std_logic;
				clk	: in	std_logic;
				g_cnt : out std_logic_vector (N-1 downto 0));
end gray_counter;

architecture beh of gray_counter is
	signal q: unsigned(N-1 downto 0);
begin

	StateReg: process(clk)
	begin
		if(clk = '1' and clk'event) then
			if(reset = '1') then
				q <= (others => '0');
			else
				q <= q + 1;
			end if;
		end if;
	end process;

	OutputProcess: process(q)
		variable tmp: std_logic_vector(N-1 downto 0);
	begin
		tmp := std_logic_vector(q) xor std_logic_vector('0' & q(N-1 downto 1));
		g_cnt <= std_logic_vector(tmp);
	end process;

end beh;
