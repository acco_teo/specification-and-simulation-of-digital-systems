----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    13:45:06 10/23/2018
-- Design Name:
-- Module Name:    mux_4to1 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_4to1 is
    port (i3, i2, i1, i0: in std_logic;
          s1, s0:         in std_logic;
          o:              out std_logic);
end mux_4to1;

architecture Behavioral of mux_4to1 is

begin
	process (i3, i2, i1, i0, s1, s0)
	begin
		if(s1='0' and s0='0')	 then o <= i0;
		elsif(s1='0' and s0='1') then o <= i1;
		elsif(s1='1' and s0='0') then o <= i2;
		elsif(s1='1' and s0='1') then o <= i3;
		else	o <= '0';
		end if;
	end process;
  
end Behavioral;
