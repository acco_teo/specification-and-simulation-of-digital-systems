----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    13:45:06 10/23/2018
-- Design Name:
-- Module Name:    mux_8to1 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- library UNISIM;
-- use UNISIM.VComponents.all;

entity mux_8to1 is
	port (i: 	IN std_logic_vector(7 downto 0);
				sel: 	IN std_logic_vector(2 downto 0);
				o: 	OUT std_logic);
end mux_8to1;

architecture Behav_CASE of mux_8to1 is

begin
	process (i, sel)
	begin
		case sel is
			when "000" => o <= i(0);
			when "001" => o <= i(1);
			when "010" => o <= i(2);
			when "011" => o <= i(3);
			when "100" => o <= i(4);
			when "101" => o <= i(5);
			when "110" => o <= i(6);
			when "111" => o <= i(7);
			when others => o <= '0';
		end case;
	end process;

end Behav_CASE;

architecture Behav_IF_ELSIF of mux_8to1 is
begin

	process (i, sel)
	begin
		if(sel = "000") then o <= i(0);
		elsif(sel = "001") then o <= i(1);
		elsif(sel = "010") then o <= i(2);
		elsif(sel = "011") then o <= i(3);
		elsif(sel = "100") then o <= i(4);
		elsif(sel = "101") then o <= i(5);
		elsif(sel = "110") then o <= i(6);
		elsif(sel = "111") then o <= i(7);
		else o <= '0';
		end if;
	end process;

end Behav_IF_ELSIF;

architecture Behav_WHEN_ELSE of mux_8to1 is
begin

	o <= i(0) when (sel="000") else
			i(1) when (sel="001") else
			i(2) when (sel="010") else
		  i(3) when (sel="011") else
		  i(4) when (sel="100") else
		  i(5) when (sel="101") else
		  i(6) when (sel="110") else
		  i(7) when (sel="111") else
		  '0';

end Behav_WHEN_ELSE;

architecture Behav_WITH_SELECT_WHEN of mux_8to1 is
begin

	with sel select
		o <= i(0) when ("000"),
			  i(1) when ("001"),
			  i(2) when ("010"),
			  i(3) when ("011"),
			  i(4) when ("100"),
			  i(5) when ("101"),
			  i(6) when ("110"),
			  i(7) when ("111"),
			  '0' when others;

end Behav_WITH_SELECT_WHEN;

architecture Circuit of mux_8to1 is

	component mux_4to1 is
		port (i3, i2, i1, i0: in std_logic;
					s1, s0:					in std_logic;
					o:						out std_logic);
	end component;

	component mux_2to1 is
		port (i1, i0: in std_logic;
 				 s:				in std_logic;
				 o:				out std_logic);
	end component;

	signal n0, n1: std_logic;

	for M_4to1_A: mux_4to1 use entity work.mux_4to1(Behavioral);
	for M_4to1_B: mux_4to1 use entity work.mux_4to1(Behavioral);
	for M_2to1: mux_2to1 use entity work.mux_2to1(Structural);

begin

	M_4to1_A: mux_4to1 port map (i(7), i(6), i(5), i(4), sel(2), sel(1), n1);
	M_4to1_B: mux_4to1 port map (i(3), i(2), i(1), i(0), sel(2), sel(1), n0);
	M_2to1: mux_2to1 port map (n1, n0, sel(0), o);

end Circuit;
