----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    05:29:23 10/29/2018
-- Design Name:
-- Module Name:    delay_models - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity delay_models is
    port ( sig_in:  in  STD_LOGIC;
           sig_out: out  STD_LOGIC_VECTOR (2 downto 0));
end delay_models;

-- sig_out(0) = no delay;
-- sig_out(1) = inertial delay;
-- sig_out(2) = transport delay;

architecture Behavioral of delay_models is

begin
	process(sig_in)
	begin
		sig_out(0) <= sig_in;
		sig_out(1) <= sig_in after 10 ns;
		sig_out(2) <= transport sig_in after 10 ns;
	end process;
  
end Behavioral;
