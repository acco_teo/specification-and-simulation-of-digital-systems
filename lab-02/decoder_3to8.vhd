----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    11:34:22 10/24/2018
-- Design Name:
-- Module Name:    decoder_3to8 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder_3to8 is
    port ( i : in  STD_LOGIC_VECTOR (2 downto 0);
           s : in  STD_LOGIC;
           d : out STD_LOGIC_VECTOR (7 downto 0));
end decoder_3to8;

architecture Behavioral_WHEN_ELSE of decoder_3to8 is

begin
	d <= "00000001"	when ((i="000") and (s='1')) else
		  "00000010"	when ((i="001") and (s='1')) else
		  "00000100"	when ((i="010") and (s='1')) else
		  "00001000"	when ((i="011") and (s='1')) else
		  "00010000"	when ((i="100") and (s='1')) else
		  "00100000"	when ((i="101") and (s='1')) else
		  "01000000"	when ((i="110") and (s='1')) else
		  "10000000"	when ((i="111") and (s='1')) else
		  "00000000";
end Behavioral_WHEN_ELSE;

architecture Behavioral_WITH_SELECT_WHEN of decoder_3to8 IS

	signal tmp_i : std_logic_vector(3 downto 0);

begin
	tmp_i <= s & i;					-- create a tmp input vector which aggregate enable signal and inputs
	with i select
		d <= "00000001" when ("1000"),
			  "00000010" when ("1001"),
			  "00000100" when ("1010"),
			  "00001000" when ("1011"),
		     "00010000" when ("1100"),
		     "00100000" when ("1101"),
		     "01000000" when ("1110"),
		     "10000000" when ("1111"),
		     "00000000" when others;
end Behavioral_WITH_SELECT_WHEN;

architecture Behavioral_CASE of decoder_3to8 is

	signal tmp_i : std_logic_vector(3 downto 0);

begin
	process(i, s)
	begin
		tmp_i <= s & i;					-- create a tmp input vector which aggregate enable signal and inputs
		case tmp_i is
			when "1000" =>	d <= "00000001";
			when "1001" =>	d <= "00000010";
			when "1010" =>	d <= "00000100";
			when "1011" =>	d <= "00001000";
			when "1100" =>	d <= "00010000";
			when "1101" =>	d <= "00100000";
			when "1110" =>	d <= "01000000";
			when "1111" =>	d <= "10000000";
			when others => d <= "00000000";
		end case;
	end process;
end Behavioral_CASE;

architecture Behavioral_IF_ELSIF of decoder_3to8 is
begin
	process(i, s)
	begin
		if(s='1' and i="000") then d <= "00000001";
		elsif(s='1' and i="001")	then d <= "00000010";
		elsif(s='1' and i="010")	then d <= "00000100";
		elsif(s='1' and i="011")	then d <= "00001000";
		elsif(s='1' and i="100")	then d <= "00010000";
		elsif(s='1' and i="101")	then d <= "00100000";
		elsif(s='1' and i="110")	then d <= "01000000";
		elsif(s='1' and i="111")	then d <= "10000000";
		else d <= "00000000";
		end if;
	end process;
end Behavioral_IF_ELSIF;
