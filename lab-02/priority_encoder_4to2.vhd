----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    16:47:48 10/24/2018
-- Design Name:
-- Module Name:    priority_encoder_4to2 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity priority_encoder_4to2 is
    port ( req : in  STD_LOGIC_VECTOR (3 downto 0);
           code : out  STD_LOGIC_VECTOR (1 downto 0);
           active : out  STD_LOGIC);
end priority_encoder_4to2;

architecture Behavioral_IF_ELSIF of priority_encoder_4to2 is
begin

	process(req)
	begin
		if(req = "1XXX") then
			code <= "11";
			active <= '1';
		elsif(req = "01XX") then
			code <= "10";
			active <= '1';
		elsif(req = "001X") then
			code <= "01";
			active <= '1';
		elsif(req = "0001") then
			code <= "00";
			active <= '1';
		else
			code <= "00";
			active <= '1';
		end if;
	end process;

end Behavioral_IF_ELSIF;

architecture Behavioral_CASE of priority_encoder_4to2 is
begin

	process(req)
	begin
		case req is
			when "1XXX" => code <= "11"; active <= '1';
			when "01XX" => code <= "10"; active <= '1';
			when "001X" => code <= "01"; active <= '1';
			when "0001" => code <= "00"; active <= '1';
			when others => code <= "00"; active <= '0';
		end case;
	end process;

end Behavioral_CASE;

architecture Behavioral_WITH_SELECT of priority_encoder_4to2 is
begin

	with req select
		code <= "11" when("1XXX"),
            "10" when("01XX"),
		        "01" when("001X"),
            "00" when("0001"),
            "00" when others;

	with req select
		active <= '1'  when "1XXX" | "01XX" | "001X" | "0001",
		          '0'	when others;

end Behavioral_WITH_SELECT;

architecture Behavioral_WHEN_ELSE of priority_encoder_4to2 is
begin

	code <= "11" when(req = "1XXX") else
          "10" when(req = "01XX") else
          "01" when(req = "001X") else
          "00" when(req = "0001") else
          "00";

	active <= '1'  when (req = "1XXX" or
                       req = "01XX" or
                       req = "001X" or
                       req = "0001") else
            '0';
end Behavioral_WHEN_ELSE;
