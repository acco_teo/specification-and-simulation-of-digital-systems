----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    13:57:40 10/23/2018
-- Design Name:
-- Module Name:    mux_2to1 - Structural
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_2to1 is
    port ( i1, i0: in  std_logic;
           s: 		 in  std_logic;
           o: 		 out  std_logic);
end mux_2to1;

architecture Structural of mux_2to1 is

	component And2 is
		Port (i0, i1: in std_logic;
				o: out std_logic);
	end component;

	component Or2 is
		Port (i0, i1: in std_logic;
				o: out std_logic);
	end component;

	component Inv is
		Port (i: in std_logic;
				o: out std_logic);
	end component;

	signal n1, n2, n3: std_logic;

begin
	Inv_1: Inv port map (s, n1);
	And2_1: And2 port map (i0, n1, n2);
	And2_2: And2 port map (i1, s, n3);
	Or2_1: Or2 port map (n2, n3, o);

end Structural;
