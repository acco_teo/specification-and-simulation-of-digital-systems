--------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:   07:28:36 10/29/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/multiplexer_8to1/mux_8to1_tb.vhd
-- Project Name:  multiplexer_8to1
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: mux_8to1
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity testbench is
end testbench;

architecture behavior of testbench is

    -- Component Declaration for the Unit Under Test (UUT)
    component mux_8to1
    port(i : in  std_logic_vector(7 downto 0);
         sel : in  std_logic_vector(2 downto 0);
         o : out  std_logic);
    end component;

   --Inputs
   signal i_s : std_logic_vector(7 downto 0);
   signal sel_s : std_logic_vector(2 downto 0);

 	--Outputs
   signal o_s : std_logic;

begin

  CompToTest: mux_8to1 port map (i_s, sel_s, o_s);

  process
	begin
	-- Test all possible input  combinations
      i_s <= "00000001"; sel_s <= "000";
      wait for 10 ns;

      i_s <= "11111101"; sel_s <= "001";
      wait for 10 ns;

      i_s <= "00000100"; sel_s <= "010";
      wait for 10 ns;

      i_s <= "11110111"; sel_s <= "011";
      wait for 10 ns;

      i_s <= "00010000"; sel_s <= "100";
      wait for 10 ns;

      i_s <= "11011111"; sel_s <= "101";
      wait for 10 ns;

      i_s <= "01000000"; sel_s <= "110";
      wait for 10 ns;

      i_s <= "01111111"; sel_s <= "111";
      wait for 10 ns;

      wait;
	end process;

end behavior;
