library ieee;
use ieee.std_logic_1164.all;

entity compl9 is 
	port(bcd: in std_logic_vector(3 downto 0);
		 c9: out std_logic_vector(3 downto 0));
end compl9;

architecture beh1 of compl9 is 
begin
	process(bcd)
	begin
		if(bcd = "0000") then
			c9 <= "1001";
		elsif(bcd = "0001") then
			c9 <= "1000";
		elsif(bcd = "0010") then
			c9 <= "0111";
		elsif(bcd = "0011") then
			c9 <= "0110";
		elsif(bcd = "0100") then
			c9 <= "0101";
		elsif(bcd = "0101") then
			c9 <= "0100";
		elsif(bcd = "0110") then
			c9 <= "0011";
		elsif(bcd = "0111") then
			c9 <= "0010";
		elsif(bcd = "1000") then
			c9 <= "0001";
		elsif(bcd = "1001") then
			c9 <= "0000";
		else
			c9 <= "ZZZZ";
		end if;
	end process;
end beh1;

architecture beh2 of compl9 is
begin
	process(bcd)
	begin
		case bcd is
			when "0000" => c9 <= "1001";
			when "0001" => c9 <= "1000";
			when "0010" => c9 <= "0111";
			when "0011" => c9 <= "0110";
			when "0100" => c9 <= "0101";
			when "0101" => c9 <= "0100";
			when "0110" => c9 <= "0011";
			when "0111" => c9 <= "0010";
			when "1000" => c9 <= "0001";
			when "1001" => c9 <= "0000";
			when others => c9 <= "ZZZZ";
		end case;
	end process;
end beh2;