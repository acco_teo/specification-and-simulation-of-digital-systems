library ieee;
use ieee.std_logic_1164.all;

entity compl9_tb is
end compl9_tb;

architecture behavior of compl9_tb is
  component compl9
    port(bcd : in  std_logic_vector(3 downto 0);
         c9 : out  std_logic_vector(3 downto 0));
  end component;

	for all: compl9 use entity work.compl9 (beh1); -- beh1 or beh2

  signal bcd_s: std_logic_vector(3 downto 0) := (others => '0');
  signal c9_s: std_logic_vector(3 downto 0);

  constant period : time := 10 ns;

begin

   uut: compl9 port map (
          bcd => bcd_s,
          c9 => c9_s
        );

   stim_proc: process
   begin
     bcd_s <= "0000";
     wait for period;
     bcd_s <= "0001";
     wait for period;
     bcd_s <= "0010";
     wait for period;
     bcd_s <= "0011";
     wait for period;
     bcd_s <= "0100";
     wait for period;
     bcd_s <= "0101";
     wait for period;
     bcd_s <= "0110";
     wait for period;
     bcd_s <= "0111";
     wait for period;
     bcd_s <= "1000";
     wait for period;
     bcd_s <= "1001";
     wait for period;
     wait;
   end process;

end;
