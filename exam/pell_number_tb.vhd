
library ieee;
use ieee.std_logic_1164.all;
 
entity pell_number_tb is
end pell_number_tb;
 
architecture behavior of pell_number_tb is 
	-- component declaration for the unit under test (uut)
	component pell_number
	port(n : in  std_logic_vector(2 downto 0);
		  start : in  std_logic;
		  clk : in  std_logic;
		  rst : in  std_logic;
		  ready : out  std_logic;
		  pell : out  std_logic_vector(7 downto 0));
    end component;
	 
	 for all: pell_number use entity work.pell_number (fsmd); -- use hlsm or fsmd
	 
   --inputs
   signal n_s : std_logic_vector(2 downto 0) := (others => '0');
   signal start_s : std_logic := '0';
   signal clk_s : std_logic := '0';
   signal rst_s : std_logic := '0';

 	--outputs
   signal ready_s : std_logic;
   signal pell_s : std_logic_vector(7 downto 0);

   -- clock period definitions
   constant clk_period : time := 10 ns;
 
begin
 
	-- instantiate the unit under test (uut)
   uut: pell_number port map (
          n => n_s,
          start => start_s,
          clk => clk_s,
          rst => rst_s,
          ready => ready_s,
          pell => pell_s
			 );

   -- clock process definitions
   clk_process :process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;
 

   -- stimulus process
   stim_proc: process
   begin		
      rst_s <= '1';
		start_s <= '0';
		wait until clk_s = '1' and clk_s'event;
		wait for 5 ns;
		
		rst_s <= '0';
		start_s <= '1';
		n_s <= "111";
		wait until clk_s = '1' and clk_s'event;
		start_s <= '0';
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		n_s <= "001";
		wait until clk_s = '1' and clk_s'event; 
		start_s <= '1';
		wait until clk_s = '1' and clk_s'event;
		start_s <= '0';
		wait until clk_s = '1' and clk_s'event;
		wait until clk_s = '1' and clk_s'event;
		n_s <= "000";
		wait until clk_s = '1' and clk_s'event; 
		start_s <= '1';
		wait until clk_s = '1' and clk_s'event;
		start_s <= '0';
		wait;
   end process;

end;
