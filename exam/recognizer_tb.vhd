library ieee;
use ieee.std_logic_1164.all;

entity recognizer_tb is
end recognizer_tb;

architecture behavior of recognizer_tb is

  component recognizer
    port(x: in  std_logic;
         clk: in  std_logic;
         rst: in  std_logic;
         en: in  std_logic;
         z: out  std_logic_vector(1 downto 0));
  end component;

  for all: recognizer use entity work.recognizer (fsm_op); -- fsm or fsm_op

  signal x_s : std_logic := '0';
  signal clk_s : std_logic := '0';
  signal rst_s : std_logic := '0';
  signal en_s : std_logic := '0';

  signal z_s : std_logic_vector(1 downto 0);

  constant clk_period : time := 10 ns;

begin

   uut: recognizer port map (
          x => x_s,
          clk => clk_s,
          rst => rst_s,
          en => en_s,
          z => z_s
        );

   -- clock process definitions
   clk_process :process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;


   -- stimulus process
   stim_proc: process
   begin
		rst_s <= '1';
		wait for clk_period/2;
		rst_s <= '0';
		wait for clk_period/2;
		x_s <= '0';
		en_s <= '0';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '0';
		wait until clk_s='1' and clk_s'event;
		x_s <= '0';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '0';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '0';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '0';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '0';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '0';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '0';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '1';
		wait until clk_s='1' and clk_s'event;
		x_s <= '1';
		en_s <= '0';
    wait;
   end process;

end;
