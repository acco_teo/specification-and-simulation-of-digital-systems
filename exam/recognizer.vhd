library ieee;
use ieee.std_logic_1164.all;

entity recognizer is
	port(x: in std_logic;
		 clk: in std_logic;
		 rst: in std_logic;
		 en: in std_logic;
		 z: out std_logic_vector(1 downto 0));
end recognizer;

architecture fsm of recognizer is
	type state_type is (idle, s1, s2, s3, s4, s5);
	signal curr_state, next_state: state_type;
begin
	
	StateReg: process(clk, rst)
	begin
		if(clk = '1' and clk'event) then
			if(rst = '1') then
				curr_state <= idle;
			else
				curr_state <= next_state;
			end if;
		end if;
	end process;

	CombLogic: process(curr_state, x, en)
	begin
		case curr_state is
			when idle => 
						z <= "00";
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
							else
								next_state <= s3;
							end if;
						else
							next_state <= idle;
						end if;
			when s1 =>
						z <= "00";
						if(en = '1') then
							if(x = '0') then
								next_state <= s2;
							else
								next_state <= s3;
							end if;
						else
							next_state <= idle;
						end if;
			when s2 =>
						z <= "01";
						if(en = '1') then
							if(x = '0') then
								next_state <= s2;
							else
								next_state <= s3;
							end if;
						else
							next_state <= idle;
						end if;
			when s3 =>
						z <= "00";
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
							else
								next_state <= s4;
							end if;
						else
							next_state <= idle;
						end if;
			when s4 =>
						z <= "00";
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
							else
								next_state <= s5;
							end if;
						else
							next_state <= idle;
						end if;
			when s5 =>
						z <= "10";
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
							else
								next_state <= s5;
							end if;
						else
							next_state <= idle;
						end if;
			end case;
	end process;
end fsm;

architecture fsm_op of recognizer is
	type state_type is (idle, s1, s2, s3);
	signal curr_state, next_state: state_type;
begin
	
	StateReg: process(clk, rst)
	begin
		if(clk = '1' and clk'event) then
			if(rst = '1') then
				curr_state <= idle;
			else
				curr_state <= next_state;
			end if;
		end if;
	end process;

	CombLogic: process(curr_state, x, en)
	begin
		case curr_state is
			when idle => 
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
								z <= "00";
							else
								next_state <= s2;
								z <= "00";
							end if;
						else
							next_state <= idle;
							z <= "00";
						end if;
			when s1 =>
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
								z <= "01";
							else
								next_state <= s2;
								z <= "00";
							end if;
						else
							next_state <= idle;
							z <= "00";
						end if;
			when s2 =>
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
								z <= "00";
							else
								next_state <= s3;
								z <= "00";
							end if;
						else
							next_state <= idle;
							z <= "00";
						end if;
			when s3 =>
						if(en = '1') then
							if(x = '0') then
								next_state <= s1;
								z <= "00";
							else
								next_state <= s3;
								z <= "10";
							end if;
						else
							next_state <= idle;
							z <= "00";
						end if;
			end case;
	end process;
end fsm_op;