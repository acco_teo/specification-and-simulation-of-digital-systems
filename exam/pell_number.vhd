library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pell_number is
	port(n: in std_logic_vector(2 downto 0);
		 start: in std_logic;
		 clk: in std_logic;
		 rst: in std_logic;
		 ready: out std_logic;
		 pell: out std_logic_vector(7 downto 0));
end pell_number;

architecture hlsm of pell_number is
	type state_type is (idle, p);
	signal curr_state, next_state: state_type;
	signal curr_pa, next_pa, curr_pb, next_pb: unsigned(7 downto 0);
	signal curr_cnt, next_cnt: unsigned(2 downto 0);
begin
	StateReg: process(clk, rst)
	begin
		if(clk = '1' and clk'event) then
			if(rst = '1') then
				curr_state <= idle;
				curr_cnt <= (others => '0');
				curr_pa <= (others => '0');
				curr_pb <= "00000001";
			else
				curr_state <= next_state;
				curr_cnt <= next_cnt;
				curr_pa <= next_pa;
				curr_pb <= next_pb;
			end if;
		end if;
	end process;

	CombLogic: process(curr_state, start, curr_cnt, curr_pb)
	begin
		case curr_state is
			when idle =>
						ready <= '0';
						next_cnt <= unsigned(n);
						next_pa <= (others => '0');
						next_pb <= "00000001";
						if(start = '1') then
							next_state <= p;
						else
							next_state <= idle;
						end if;
			when p =>
						if(curr_cnt >= 1) then
							ready <= '0';
							next_cnt <= curr_cnt - 1;
							next_pa <= curr_pb;
							next_pb <= resize(2*curr_pb + curr_pa, 8);
							next_state <= p;
						else
							ready <= '1';
							next_cnt <= (others => '0');
							next_pa <= (others => '0');
							next_pb <= "00000001";
							next_state <= idle;
						end if;
		end case;
	end process;

	pell <= std_logic_vector(curr_pb) when(curr_cnt >= 1) else
			  std_logic_vector(curr_pa);

end hlsm;

architecture fsmd of pell_number is
	type state_type is (idle, p);
	signal curr_state, next_state: state_type;
	signal curr_pa, next_pa, curr_pb, next_pb: unsigned(7 downto 0);
	signal curr_cnt, next_cnt: unsigned(2 downto 0);
	signal cnt_ld, cnt_s, cnt_eq: std_logic;
	signal pell_ld, pell_s: std_logic;
begin
	-- Controller
	CtrlReg: process(clk, rst)
	begin
		if(clk = '1' and clk'event) then
			if(rst = '1') then
				curr_state <= idle;
			else
				curr_state <= next_state;
			end if;
		end if;
	end process;

	CtrlLogic: process(curr_state, start, cnt_eq, curr_pb)
	begin
		case curr_state is
			when idle =>
						ready <= '0';
						pell <= std_logic_vector(curr_pa);
						cnt_s <= '0';
						pell_s <= '0';
						cnt_ld <= '1';
						pell_ld <= '1';
						if(start = '1') then
							next_state <= p;
						else
							next_state <= idle;
						end if;
			when p =>
						if(cnt_eq = '0') then
							ready <= '0';
							cnt_s <= '1';
							pell_s <= '1';
							cnt_ld <= '1';
							pell_ld <= '1';
							pell <= std_logic_vector(curr_pb);
							next_state <= p;
						else
							ready <= '1';
							cnt_s <= '0';
							pell_s <= '0';
							cnt_ld <= '1';
							pell_ld <= '1';
							if(unsigned(n) > 0) then
								pell <= std_logic_vector(curr_pb);
							else
								pell <= std_logic_vector(curr_pa);
							end if;
							next_state <= idle;
						end if;
		end case;
	end process;

	-- Datapath
	DpReg: process(clk, rst, cnt_ld, pell_ld)
	begin
		if(clk = '1' and clk'event) then
			if(rst = '1') then
				curr_cnt <= (others => '0');
				curr_pa <= (others => '0');
				curr_pb <= "00000001";
			else
				if(pell_ld = '1') then
					curr_pa <= next_pa;
					curr_pb <= next_pb;
				end if;
				if(cnt_ld = '1') then
					curr_cnt <= next_cnt;
				end if;
			end if;
		end if;
	end process;

	DpLogic: process(cnt_s, pell_s, curr_cnt, curr_pa, curr_pb, n)
	begin
		if(pell_s = '1') then
			next_pa <= curr_pb;
			next_pb <= resize(2*curr_pb + curr_pa, 8);
		else
			next_pa <= (others => '0');
			next_pb <= "00000001";
		end if;

		if(cnt_s = '1') then
			next_cnt <= curr_cnt - 1;
		else
			next_cnt <= unsigned(n);
		end if;

		if(curr_cnt > 1) then
			cnt_eq <= '0';
		else
			cnt_eq <= '1';
		end if;
	end process;
end fsmd;
