----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    14:54:53 11/27/2018
-- Design Name:
-- Module Name:    edge_detection_circuit - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity edge_detection_circuit is
	port (strobe: in  std_logic;
				clk	: in  std_logic;
				rst	: in  std_logic;
				pulse : out std_logic);
end edge_detection_circuit;

architecture behavioral of edge_detection_circuit is
	type state_type is (s0, s1, s2);
	signal next_state, curr_state: state_type;
begin

	StateReg: process(rst, clk)
	begin
		if(rst = '1') then
			curr_state <= s0;
		elsif(clk'event and clk = '1') then
			curr_state <= next_state;
		end if;
	end process;

	CombLogic: process(strobe, curr_state)
	begin
		case curr_state is
			when s0 =>
						pulse <= '0';
						if(strobe = '1') then
							next_state <= s1;
						else
							next_state <= s0;
						end if;
			when s1 =>
						pulse <= '1';
						if(strobe = '1') then
							next_state <= s2;
						else
							next_state <= s0;
						end if;
			when s2 =>
						pulse <= '0';
						if(strobe = '1') then
							next_state <= s2;
						else
							next_state <= s0;
						end if;
			when others =>
						pulse <= 'Z';
						next_state <= s0;
			end case;
	end process;

end behavioral;
