--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   18:38:54 12/09/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-07/arbiter_2req/arbitier_2req_tb.vhd
-- Project Name:  arbiter_2req
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: arbiter_2req_*
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity arbitier_2req_tb is
end arbitier_2req_tb;

architecture behavior of arbitier_2req_tb is

    -- Component Declaration for the Unit Under Test (UUT)
    component arbiter_2req_v1
      port (req0 : IN  std_logic;
            req1 : IN  std_logic;
            clk : IN  std_logic;
            rst : IN  std_logic;
            grant0 : OUT  std_logic;
            grant1 : OUT  std_logic);
    end component;

    component arbiter_2req_v2
      port (req0 : IN  std_logic;
            req1 : IN  std_logic;
            clk : IN  std_logic;
            rst : IN  std_logic;
            grant0 : OUT  std_logic;
            grant1 : OUT  std_logic);
    end component;



   --Inputs
   signal req0_s : std_logic := '0';
   signal req1_s : std_logic := '0';
   signal clk_s : std_logic := '0';
   signal rst_s : std_logic := '0';

 	--Outputs
   signal grant0_s : std_logic;
   signal grant1_s : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
--   uut1: arbiter_2req_v1 port map (
--          req0 => req0,
--          req1 => req1,
--          clk => clk,
--          rst => rst,
--          grant0 => grant0,
--          grant1 => grant1
--        );

	uut2: arbiter_2req_v2 port map (
          req0 => req0_s,
          req1 => req1_s,
          clk => clk_s,
          rst => rst_s,
          grant0 => grant0_s,
          grant1 => grant1_s
        );

   -- Clock process definitions
   clk_process :process
   begin
     clk_s <= '0';
     wait for clk_period/2;
     clk_s <= '1';
     wait for clk_period/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin
     rst_s <= '1';
     wait for 3 ns;
     rst_s <= '0';
     req0_s <= '0';
     req1_s <= '0';
     wait until clk_s='1' and clk_s'event;
     wait for 5 ns;
     req0_s <= '1';
     req1_s <= '0';
     wait until clk_s='1' and clk_s'event;
     wait for 5 ns;
     req0_s <= '0';
     req1_s <= '0';
     wait until clk_s='1' and clk_s'event;
     wait for 5 ns;
     req0_s <= '1';
     req1_s <= '1';
     wait until clk_s='1' and clk_s'event;
     wait for 5 ns;
     req0_s <= '0';
     req1_s <= '0';
     wait until clk_s='1' and clk_s'event;
     wait for 5 ns;
     req0_s <= '1';
     req1_s <= '1';
     wait;
   end process;

end;
