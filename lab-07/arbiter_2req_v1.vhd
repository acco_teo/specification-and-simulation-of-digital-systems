----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accorneo
--
-- Create Date:    22:26:06 11/28/2018
-- Design Name:
-- Module Name:    arbiter_2req_v1 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity arbiter_2req_v1 is
	port (req0	 : in  std_logic;
				req1	 : in  std_logic;
				clk	 : in std_logic;
				rst	 : in  std_logic;
				grant0 : out  std_logic;
				grant1 : out  std_logic);
end arbiter_2req_v1;

architecture behavioral of arbiter_2req_v1 is
	type state_type is (free, subs0, subs1);
	signal next_state, curr_state: state_type;
begin

	StateReg: process(rst, clk)
	begin
		if(rst = '1') then
			curr_state <= free;
		elsif(clk'event and clk = '1') then
			curr_state <= next_state;
		end if;
	end process;

	CombLogic: process(req0, req1, curr_state)
	begin
		case curr_state is
			when free => -- resource free
							grant0 <= '0';
							grant1 <= '0';
							if(req0 = '1') then
								next_state <= subs0;
							elsif(req0 = '0' and req1 = '1') then
								next_state <= subs1;
							else
								next_state <= free;
							end if;
			when subs0 => -- resource locked by subsystem0
							grant0 <= '1';
							grant1 <= '0';
							if(req0 = '0') then
								next_state <= free;
							else
								next_state <= subs0;
							end if;
			when subs1 => -- resource locked by subsystem1
							grant0 <= '0';
							grant1 <= '1';
							if(req0 = '1') then
								next_state <= subs0;
							elsif(req1 = '0') then
								next_state <= free;
							else
								next_state <= subs1;
							end if;
			when others => -- safe state
							grant0 <= '0';
							grant1 <= '0';
							next_state <= free;
			end case;
	end process;

end behavioral;
