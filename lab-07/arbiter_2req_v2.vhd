----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    09:43:17 11/30/2018
-- Design Name:
-- Module Name:    arbiter_2req_v2 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity arbiter_2req_v2 is
	port (req0	 : in  std_logic;
				req1	 : in  std_logic;
				clk	 : in std_logic;
				rst	 : in  std_logic;
				grant0 : out  std_logic;
				grant1 : out  std_logic);
end arbiter_2req_v2;

architecture behavioral of arbiter_2req_v2 is
	type state_type is (free, r0, r1);
	signal next_state, curr_state: state_type;
	signal new_used, last_used: std_logic;
begin

	StateReg: process(rst, clk)
	begin
		if(rst = '1') then
			curr_state <= free;
			last_used <= 'X'; -- assuming that @ t0 only one request is rised
		elsif(clk'event and clk = '1') then
			curr_state <= next_state;
			last_used <= new_used;
		end if;
	end process;

	CombLogic: process(req0, req1, curr_state, last_used)
	begin
		case curr_state is
			when free => -- resource free
						grant0 <= '0';
						grant1 <= '0';
						new_used <= last_used;
						if((req0 = '1' and req1 = '1' and last_used = '1') or
							(req0 = '1' and req1 = '0')) then
								next_state <= r0;
						elsif((req0 = '1' and req1 = '1' and last_used = '0') or
							(req0 = '0' and req1 = '1')) then
								next_state <= r1;
						else
								next_state <= free;
						end if;
			when r0 => -- resource locked by subsystem0
						grant0 <= '1';
						grant1 <= '0';
						new_used <= '0';
						if(req0 = '0') then
							next_state <= free;
						else
							next_state <= r0;
						end if;
			when r1 => -- resource locked by subsystem1
						grant0 <= '0';
						grant1 <= '1';
						new_used <= '1';
						if(req1 = '0') then
							next_state <= free;
						else
							next_state <= r1;
						end if;
			when others => -- safe state
							grant0 <= '0';
							grant1 <= '0';
							new_used <= last_used;
							next_state <= free;
			end case;
	end process;

end behavioral;
