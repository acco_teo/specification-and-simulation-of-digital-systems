--------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:   12:23:11 11/08/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-03/gray_code/gray_code_tb.vhd
-- Project Name:  gray_code
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: gray_code
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity gray_tb is
end gray_tb;

architecture tb_arch_gray of gray_tb is

	component gray
		generic (N : integer := 4);
		port(input : in std_logic_vector(N-1 downto 0);
			  output : out std_logic_vector(N-1 downto 0));
	end component;

	constant DELAY : time := 10 ns;
	constant N : integer := 4;

	signal input_t, output_t : std_logic_vector(N-1 downto 0);

begin

	UUT : entity work.gray_code(dataflow) generic map(N) port map(input_t, output_t);

	process
	begin
		input_t <= "0100";
		wait for DELAY;
		input_t <= "1000";
		wait for DELAY;
		wait;
	end process;
	
end tb_arch_gray;
