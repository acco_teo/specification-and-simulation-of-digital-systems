----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    13:42:33 10/30/2018
-- Design Name:
-- Module Name:    hamming_distance - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity hamming_distance is
    port ( word_1: in std_logic_vector (7 downto 0);
           word_2: in std_logic_vector (7 downto 0);
           h_distance: out std_logic_vector (3 downto 0));
end hamming_distance;

--architecture behavioral of hamming_distance is
--begin
--	process(word_1, word_2)
--	variable sum: unsigned(3 downto 0);
--	begin
--		sum := "0000";
--		for i in word_1'low to word_1'high loop
--			if(word_1(i) /= word_2(i)) then
--				sum := sum + 1;
--			end if;
--		end loop;
--		h_distance <= std_logic_vector(sum(3 downto 0));
--	end process;
--end behavioral;

architecture structural of hamming_distance is

	component full_adder is
		port (a: in std_logic;
          b: in std_logic;
          cin: in std_logic;
          sum: out std_logic;
          carry: out std_logic);
	end component;

	component half_adder is
		port (a: in std_logic;
          b: in std_logic;
          sum: out std_logic;
          carry: out std_logic);
	end component;

	component Xor2 is
		port (x, y: in std_logic;
          F:	out std_logic);
	end component;

	signal n: std_logic_vector(7 downto 0); -- xor exits, first layer adders inputs
	signal s: std_logic_vector(7 downto 0); -- adders sum exit
	signal c: std_logic_vector(9 downto 0); -- adders carry exit
	signal t: std_logic_vector(3 downto 0); -- tmp exit for hamming code

begin

	xor_gen: for i in word_1'low to word_1'high generate
		x0: Xor2 port map(x => word_1(i), y => word_2(i), F => n(i));
	end generate xor_gen;

	-- pattern: name component + component number

	-- first layer half adders
	ha0: half_adder port map(a => n(0), b => n(1), sum => s(0), carry => c(0));
	ha1: half_adder port map(a => n(2), b => n(3), sum => s(1), carry => c(1));
	ha2: half_adder port map(a => n(4), b => n(5), sum => s(2), carry => c(2));
	ha3: half_adder port map(a => n(6), b => n(7), sum => s(3), carry => c(3));

	-- second layer full adders
	fa4: full_adder port map(a => s(0), b => s(1), cin =>  '0', sum => s(4), carry => c(4));
	fa5: full_adder port map(a => c(0), b => c(1), cin => c(4), sum => s(5), carry => c(5));
	fa6: full_adder port map(a => s(2), b => s(3), cin =>  '0', sum => s(6), carry => c(6));
	fa7: full_adder port map(a => c(2), b => c(3), cin => c(6), sum => s(7), carry => c(7));

	-- third layer full adders
	fa8: full_adder port map(a => s(4), b => s(6), cin =>  '0', sum => t(0), carry => c(8));
	fa9: full_adder port map(a => s(5), b => s(7), cin => c(8), sum => t(1), carry => c(9));
	fa10:full_adder port map(a => c(5), b => c(7), cin => c(9), sum => t(2), carry => t(3));

	h_distance <= t;

end structural;
