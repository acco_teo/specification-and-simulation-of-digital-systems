--------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:   04:41:09 11/05/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-03/right_barrel_shifter/right_barell_shifter_tb.vhd
-- Project Name:  right_barrel_shifter
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: right_barrell_shifter
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity testbench is
end testbench;

architecture behavior OF testbench IS

    -- Component Declaration for the Unit Under Test (UUT)

    component right_barrell_shifter
    port ( data_i	 : in  STD_LOGIC_VECTOR (7 downto 0);
           lar : in  STD_LOGIC_VECTOR (1 downto 0);
           amt : in  STD_LOGIC_VECTOR (2 downto 0);
           data_o : out  STD_LOGIC_VECTOR (7 downto 0));
    end component;


   --Inputs
   signal data_i_s : std_logic_vector(7 downto 0);
   signal lar_s : std_logic_vector(1 downto 0);
   signal amt_s : std_logic_vector(2 downto 0);

 	--Outputs
   signal data_o_s : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with
   -- appropriate port name

begin

	CompToTest: right_barrell_shifter port map (data_i_s, lar_s, amt_s, data_o_s);
	process
	begin
	-- Test all possible input  combinations
      data_i_s <= "01010101"; lar_s <= "00"; amt_s <= "011";
      wait for 10 ns;

      data_i_s <= "11010101"; lar_s <= "01"; amt_s <= "100";
      wait for 10 ns;

      data_i_s <= "11010101"; lar_s <= "10"; amt_s <= "110";
      wait for 10 ns;

		data_i_s <= "11010101"; lar_s <= "11"; amt_s <= "110";
      wait for 10 ns;

		wait;
		end process;

end behavior;
