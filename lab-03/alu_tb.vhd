--------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:   10:14:22 11/06/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-03/alu/alu_tb.vhd
-- Project Name:  alu
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: alu
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity alu_tb is
end alu_tb;

architecture tb_arch_alu of alu_tb is

	component alu is
		generic (N: integer := 4);
		port(src0 : in std_logic_vector(N-1 downto 0);
				 src1 : in std_logic_vector(N-1 downto 0);
				 ctrl : in std_logic_vector(2 downto 0);
				 rslt : out std_logic_vector(N-1 downto 0));
	end component;

	constant N : integer := 8;

	signal src0_t, src1_t, rslt_t : std_logic_vector(n-1 downto 0);
	signal ctrl_t : std_logic_vector(2 downto 0);


begin

	UUT : entity work.alu(behavioral) generic map(N) port map(src0_t, src1_t, ctrl_t, rslt_t);

	process
	begin
		ctrl_t <= "011";
		src0_t <= "11111111";
		src1_t <= "00000000";
		wait for 10 ns;
		assert rslt_t = "00000000" report "Increment failed";

		ctrl_t <= "111";
		src0_t <= "00001010";
		src1_t <= "00000110";
		wait for 10 ns;
		assert rslt_t = "00001110" report "Or failed";

		ctrl_t <= "101";
		src0_t <= "00001000";
		src1_t <= "00000111";
		wait for 10 ns;
		assert rslt_t = "00000001" report "Subtraction failed";

		ctrl_t <= "100";
		src0_t <= "00001000";
		src1_t <= "00001000";
		wait for 10 ns;
		assert rslt_t = "00010000" report "Sum failed";
		wait;
	end process;

end tb_arch_alu;
