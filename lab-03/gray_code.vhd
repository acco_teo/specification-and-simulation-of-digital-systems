----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    12:15:42 11/08/2018
-- Design Name:
-- Module Name:    gray_code - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity gray_code is
	generic (N : integer := 4);
	port(input : in std_logic_vector(N-1 downto 0);
			 output : out std_logic_vector(N-1 downto 0));
end gray_code;

architecture dataflow of gray_code is
	signal binary, binary_increment: std_logic_vector(N-1 downto 0);

begin
	binary <= input xor ('0' & binary(N-1 downto 1));
	binary_increment <= std_logic_vector(unsigned(binary) + 1);
	output <= binary_increment xor ('0' & binary_increment(N-1 downto 1));
end dataflow;
