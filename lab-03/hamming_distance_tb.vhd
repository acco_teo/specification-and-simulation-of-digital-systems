--------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:   14:11:45 10/30/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-03/hamming_distance/hamming_distance_tb.vhd
-- Project Name:  hamming_distance
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: hamming_distance
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity hamming_distance_tb is
end hamming_distance_tb;

architecture behavior OF hamming_distance_tb IS

    component hamming_distance
    port(word_1 : in std_logic_vector(7 downto 0);
         word_2 : in std_logic_vector(7 downto 0);
         h_distance : out std_logic_vector(3 downto 0));
	end component;

  --Inputs
   signal w1_s : std_logic_vector(7 downto 0);
   signal w2_s : std_logic_vector(7 downto 0);

 	--Outputs
   signal h_s : std_logic_vector(3 downto 0);

begin

	UUT : entity work.hamming_distance(structural) port map(w1_s, w2_s, h_s);

	process

		variable tmp1, tmp2: std_logic_vector(7 downto 0);

	begin
		tmp1 := "00000000";
		tmp2 := "00000000";
    w1_s <= tmp1; w2_s <= tmp2;
    WAIT FOR 10 ns;

		tmp1 := "01000000";
		tmp2 := "00000000";
    w1_s <= tmp1; w2_s <= tmp2;
    WAIT FOR 10 ns;

		tmp1 := "01000000";
		tmp2 := "00000000";
    w1_s <= tmp1; w2_s <= tmp2;
    WAIT FOR 10 ns;

		tmp1 := "00000000";
		tmp2 := "01000010";
    w1_s <= tmp1; w2_s <= tmp2;
    WAIT FOR 10 ns;

		tmp1 := "10000010";
		tmp2 := "00001000";
    w1_s <= tmp1; w2_s <= tmp2;
    wait for 10 ns;

		tmp1 := "01000010";
		tmp2 := "00101000";
    w1_s <= tmp1; w2_s <= tmp2;
    wait for 10 ns;

		tmp1 := "10100010";
		tmp2 := "01001000";
    w1_s <= tmp1; w2_s <= tmp2;
    wait for 10 ns;

		tmp1 := "10100100";
		tmp2 := "01010001";
    w1_s <= tmp1; w2_s <= tmp2;
    wait for 10 ns;

		tmp1 := "10101010";
		tmp2 := "01010100";
    w1_s <= tmp1; w2_s <= tmp2;
    wait for 10 ns;

		tmp1 := "01010101";
		tmp2 := "10101010";
    w1_s <= tmp1; w2_s <= tmp2;
    wait for 10 ns;

		wait;
		end process;
END behavior;
