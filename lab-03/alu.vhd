----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    09:54:26 11/06/2018
-- Design Name:
-- Module Name:    alu - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu is
	generic (N: integer := 4);
	port (src0 : in  std_logic_vector (N-1 downto 0);
				src1 : in  std_logic_vector (N-1 downto 0);
				ctrl : in  std_logic_vector (2 downto 0);
				rslt : out std_logic_vector (N-1 downto 0));
end alu;

architecture behavioral of alu is
begin

	process (src0, src1, ctrl)
	begin
		case ctrl is
			when "100" => rslt <= std_logic_vector(signed(src0) + signed(src1));
			when "101" => rslt <= std_logic_vector(signed(src0) - signed(src1));
			when "110" => rslt <= src0 and src1;
			when "111" => rslt <= src0 or src1;
			when others => rslt <= std_logic_vector(signed(src0) + 1);
		end case;
	end process;

end behavioral;
