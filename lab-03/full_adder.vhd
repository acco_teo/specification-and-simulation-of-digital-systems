----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    09:19:36 10/31/2018
-- Design Name:
-- Module Name:    full_adder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity full_adder is
	port (a : in std_logic;
				b : in std_logic;
				cin : in std_logic;
				sum : out std_logic;
				carry : out std_logic);
end full_adder;

architecture behavior of full_adder is
begin

	sum <= a xor b xor cin;
	carry <= (a and b) or (cin and (a xor b));

end behavior;
