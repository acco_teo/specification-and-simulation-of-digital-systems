----------------------------------------------------------------------------------
-- Company:
-- Engineer: Accornero Matteo
--
-- Create Date:    03:45:34 11/05/2018
-- Design Name:
-- Module Name:    right_barrell_shifter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity right_barrell_shifter is
    Port ( data_i	 : in  STD_LOGIC_VECTOR (7 downto 0);
           lar : in  STD_LOGIC_VECTOR (1 downto 0);
           amt : in  STD_LOGIC_VECTOR (2 downto 0);
           data_o : out  STD_LOGIC_VECTOR (7 downto 0));
end right_barrell_shifter;

architecture Behavioral of right_barrell_shifter is

	signal tmp: std_logic_vector(7 downto 0);

begin

  process(data_i, lar)
	begin
			case lar is
				when "00" => tmp <= (others => '0');
				when "01" => tmp <= (others => data_i(7));
				when others => tmp <= data_i;
			end case;
	end process;

	process(data_i, tmp, amt)
	begin
			case amt is
				when "001" => data_o <= tmp(0) & data_i(7 downto 1);
				when "010" => data_o <= tmp(1 downto 0) & data_i(7 downto 2);
				when "011" => data_o <= tmp(2 downto 0) & data_i(7 downto 3);
				when "100" => data_o <= tmp(3 downto 0) & data_i(7 downto 4);
				when "101" => data_o <= tmp(4 downto 0) & data_i(7 downto 5);
				when "110" => data_o <= tmp(5 downto 0) & data_i(7 downto 6);
				when "111" => data_o <= tmp(6 downto 0) & data_i(7);
				when others => data_o <= data_i;
			end case;
	end process;

end Behavioral;
