----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    17:26:55 11/08/2018
-- Design Name:
-- Module Name:    mul_Nx1 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mul_Nx1 is
	port (op1: in std_logic_vector(7 downto 0);
				op2: in std_logic;
				mul: out std_logic_vector(15 downto 0));
end entity;

architecture mul_Nx1_beh of mul_Nx1 is
begin

	process(op1, op2)
		variable tmp: std_logic_vector(7 downto 0);
	begin
		for i in op1'low to op1'high loop
			tmp(i) := op1(i) and op2;
		end loop;
		mul <= X"00" & tmp;
	end process;

end mul_Nx1_beh;
