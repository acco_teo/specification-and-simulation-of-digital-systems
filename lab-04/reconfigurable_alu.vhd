----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    09:06:04 11/13/2018
-- Design Name:
-- Module Name:    reconfigurable_alu - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity reconfigurable_alu is
	port (op_a	 : in  std_logic_vector (15 downto 0);
				op_b 	 : in  std_logic_vector (15 downto 0);
				ctrl : in  std_logic_vector (2 downto 0);
				nibble : in  std_logic_vector (3 downto 0);
				c_in   : in std_logic;
				c_out  : out std_logic;
				result : out std_logic_vector (15 downto 0));
end reconfigurable_alu;

architecture struct of reconfigurable_alu is

	component alu is
		generic (N: integer := 16);
		port (src0 : in  std_logic_vector (N-1 downto 0);
					src1 : in  std_logic_vector (N-1 downto 0);
					ctrl : in  std_logic_vector (2 downto 0);
					enbl : in  std_logic;
					c_in : in  std_logic;
					c_out: out std_logic;
					rslt : out std_logic_vector (N-1 downto 0));
	end component;

	signal n0, n1, n2: std_logic; -- carries
begin

		alu_0: alu generic map(N=>4) port map (src0 => op_a(3 downto 0),
															src1 => op_b(3 downto 0),
															ctrl => ctrl,
															enbl => nibble(0),
															c_in => c_in,
															c_out => n0,
															rslt => result(3 downto 0));

		alu_1: alu generic map(N=>4) port map (src0 => op_a(7 downto 4),
															src1 => op_b(7 downto 4),
															ctrl => ctrl,
															enbl => nibble(1),
															c_in => n0,
															c_out => n1,
															rslt => result(7 downto 4));

		alu_2: alu generic map(N=>4) port map (src0 => op_a(11 downto 8),
															src1 => op_b(11 downto 8),
															ctrl => ctrl,
															enbl => nibble(2),
															c_in => n1,
															c_out => n2,
															rslt => result(11 downto 8));

		alu_3: alu generic map(N=>4) port map (src0 => op_a(15 downto 12),
															src1 => op_b(15 downto 12),
															ctrl => ctrl,
															enbl => nibble(3),
															c_in => n2,
															c_out => c_out,
															rslt => result(15 downto 12));
end struct;
