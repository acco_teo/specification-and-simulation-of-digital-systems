--------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:   09:53:40 11/09/2018
-- Design Name:
-- Module Name:   C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/Lab-04/combinational-adder-based-multiplier/mul_tb.vhd
-- Project Name:  combinational-adder-based-multiplier
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: combinational_adder_based_multiplier
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

entity mul_tb is
end mul_tb;

architecture behavior of mul_tb is

    -- Component Declaration for the Unit Under Test (UUT)

    component combinational_adder_based_multiplier
    port( a : in  std_logic_vector(7 downto 0);
			 b : in  std_logic_vector(7 downto 0);
			 y : out  std_logic_vector(15 downto 0));
    end component;


   --Inputs
   signal a_s : std_logic_vector(7 downto 0) := (others => '0');
   signal b_s : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal y_s : std_logic_vector(15 downto 0);

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: combinational_adder_based_multiplier port map (
          a => a_s,
          b => b_s,
          y => y_S
        );

   -- Stimulus process
   stim_proc: process
   begin
		-- mul 1
		a_s <= "11111111";
		b_s <= "00000000";
		wait for 10 ns;
		assert y_s = X"0000" report "mul 1 failed";

		-- mul 2
		a_s <= "11111111";
		b_s <= "00000001";
		wait for 10 ns;
		assert y_s = "0000000011111111" report "mul 2 failed";

		-- mul 3
		a_s <= "10101010";
		b_s <= "01010101";
		wait for 10 ns;
		assert y_s = "0011100001110010" report "mul 3 failed";

		-- mul 4
		a_s <= "11110000";
		b_s <= "10101010";
		wait for 10 ns;
		assert y_s = "1001111101100000" report "mul 4 failed";

		-- mul 5
		a_s <= "11001100";
		b_s <= "11111111";
		wait for 10 ns;
		assert y_s = "1100101100110100" report "mul 5 failed";

		-- mul 6
		a_s <= "11111111";
		b_s <= "11111111";
		wait for 10 ns;
		assert y_s = "1111111000000001" report "mul 5 failed";

		wait;
   end process;

end behavior;
