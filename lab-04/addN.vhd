----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    15:55:02 11/08/2018
-- Design Name:
-- Module Name:    addN - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity addN is
	port(op1, op2: in std_logic_vector(15 downto 0);
			 sum: out std_logic_vector(15 downto 0));
end entity;

architecture addN_df of addN is
begin

	sum <= std_logic_vector(unsigned(op1) + unsigned(op2));

end addN_df;
