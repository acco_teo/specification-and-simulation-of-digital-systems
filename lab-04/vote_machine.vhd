----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    15:25:38 11/06/2018
-- Design Name:
-- Module Name:    vote_machine - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity vote_machine is
    port (rslt0 : in  std_logic_vector (7 downto 0);
          rslt1 : in  std_logic_vector (7 downto 0);
          rslt2 : in  std_logic_vector (7 downto 0);
          data_valid : out  std_logic;
          final_rslt : out  std_logic_vector (7 downto 0));
end vote_machine;

architecture behavioral of vote_machine is

begin
	process(rslt0, rslt1, rslt2)
	begin
		if(rslt0 = rslt1 and rslt1 = rslt2) then
			data_valid <= '1';
			final_rslt <= rslt0;
		elsif(rslt0 = rslt1 or rslt0 = rslt1) then
			data_valid <= '1';
			final_rslt <= rslt0;
		elsif(rslt1 = rslt2) then
			data_valid <= '1';
			final_rslt <= rslt1;
		else
			data_valid <= '0';
			final_rslt <= "ZZZZZZZZ";
		end if;
	end process;
end behavioral;
