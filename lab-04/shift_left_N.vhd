----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    22:30:03 11/08/2018
-- Design Name:
-- Module Name:    shift_left_N - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_left_N is
    port (data_i : in  std_logic_vector (15 downto 0);
          amt : in  std_logic_vector(2 downto 0);
          data_o : out std_logic_vector (15 downto 0));

end entity;

architecture beh of shift_left_N is
begin

	with amt select
		data_o <= data_i                             		     when "000",
              data_i(14 downto 0) & data_i(15)           when "001",
              data_i(13 downto 0) & data_i(15 downto 14) when "010",
              data_i(12 downto 0) & data_i(15 downto 13) when "011",
              data_i(11 downto 0) & data_i(15 downto 12) when "100",
              data_i(10 downto 0) & data_i(15 downto 11) when "101",
              data_i( 9 downto 0) & data_i(15 downto 10) when "110",
              data_i( 8 downto 0) & data_i(15 downto  9) when others;

end beh;
