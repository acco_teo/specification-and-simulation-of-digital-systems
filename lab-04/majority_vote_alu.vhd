----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    15:10:21 11/06/2018
-- Design Name:
-- Module Name:    majority_vote_alu - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity majority_vote_alu is
    port ( A : in  std_logic_vector (7 downto 0);
           B : in  std_logic_vector (7 downto 0);
           C : in  std_logic_vector (7 downto 0);
           D : in  std_logic_vector (7 downto 0);
           E : in  std_logic_vector (7 downto 0);
           F : in  std_logic_vector (7 downto 0);
           ctrl : in  std_logic_vector (2 downto 0);
           rslt : out  std_logic_vector (7 downto 0);
           data_valid : out  std_logic);
end majority_vote_alu;

architecture structural of majority_vote_alu is

	component alu is
		generic (N: integer := 8);
		port (src0 : in  std_logic_vector (N-1 downto 0);
          src1 : in  std_logic_vector (N-1 downto 0);
          ctrl : in  std_logic_vector (2 downto 0);
          rslt : out std_logic_vector (N-1 downto 0));
	end component;

	component vote_machine is
		port (rslt0 : in  std_logic_vector (7 downto 0);
          rslt1 : in  std_logic_vector (7 downto 0);
          rslt2 : in  std_logic_vector (7 downto 0);
          data_valid : out  std_logic;
          final_rslt : out  std_logic_vector (7 downto 0));
	end component;

	signal n0, n1, n2: std_logic_vector(7 downto 0);

begin

	alu0: alu port map(src0 => A, src1 => B, ctrl => ctrl, rslt => n0);
	alu1: alu port map(src0 => C, src1 => D, ctrl => ctrl, rslt => n1);
	alu2: alu port map(src0 => E, src1 => F, ctrl => ctrl, rslt => n2);

	cmpt: vote_machine port map(rslt0 => n0,
                              rslt1 => n1,
                              rslt2 => n2,
										          data_valid => data_valid,
                              final_rslt => rslt);

end structural;
