----------------------------------------------------------------------------------
-- Company:
-- Engineer: Matteo Accornero
--
-- Create Date:    15:41:05 11/08/2018
-- Design Name:
-- Module Name:    combinational_adder_based_multiplier - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity combinational_adder_based_multiplier is
    port ( a : in  std_logic_vector (7 downto 0);
           b : in  std_logic_vector (7 downto 0);
           y : out std_logic_vector (15 downto 0));
end combinational_adder_based_multiplier;

architecture dataflow of combinational_adder_based_multiplier is

	component mul_Nx1 is
		port(op1: in std_logic_vector(7 downto 0);
         op2: in std_logic;
         mul: out std_logic_vector(15 downto 0));
	end component;

	component shift_left_N is
		port (data_i : in  std_logic_vector (15 downto 0);
          amt : in  std_logic_vector(2 downto 0);
          data_o : out std_logic_vector (15 downto 0));
	end component;

	component addN is
		port(op1, op2: in std_logic_vector(15 downto 0);
         sum: out std_logic_vector(15 downto 0));
	end component;

	type sixteen_bit_vector_array_type is array (natural range <>) of std_logic_vector(15 downto 0);
	signal sum : sixteen_bit_vector_array_type(6 downto 0);
	signal shift_mul : sixteen_bit_vector_array_type(7 downto 0);
	signal mul : sixteen_bit_vector_array_type(7 downto 0);
begin

	mul_Nx1_gen:
	for i in a'low to a'high generate
		mul_n_x: mul_Nx1 port map(op1 => a, op2 => b(i), mul => mul(i));
	end generate;

	shift_left_N_gen:
	for i in a'low to a'high generate
		shift_l_x: shift_left_N port map(data_i => mul(i),
													amt => std_logic_vector(to_unsigned(i, 3)),
													data_o => shift_mul(i));
	end generate;


	add_N_gen:
	for i in a'low to a'high-1 generate
		g0: if i=0 generate
			add_n_0: addN port map(op1 => shift_mul(0), op2 => shift_mul(1), sum => sum(0));
		end generate g0;
		g1: if i>0 and i<=(a'high-1) generate
			add_n_x: addN port map(op1 => sum(i-1), op2 => shift_mul(i+1), sum => sum(i));
		end generate g1;
	end generate add_N_gen;

	y <= sum(a'high-1);
end dataflow;
