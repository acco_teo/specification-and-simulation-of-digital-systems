--------------------------------------------------------------------------------
-- Company: 
-- Engineer:		Matteo Accornero
--
-- Create Date:
-- Design Name:   
-- Module Name:   add_core_tb.vhd
-- Project Name:  [SSDS] Mini-Assignment
-- Target Device:  
-- Tool versions:  
-- Description:   providing some meaningful test vectors for add_core
-- 
-- VHDL Test Bench Created for module: top_level_add
-- 
-- Dependencies:	vectors.txt 
-- 					expected.txt		
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use STD.TEXTIO.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;
 
entity add_core_tb is
end add_core_tb;
 
architecture behavior of add_core_tb is 
	
	-- Component Declaration for the Unit Under Test (UUT)
	component top_level_add
	port( a : in std_logic_vector(31 downto 0);
			b : in std_logic_vector(31 downto 0);
			clk: in std_logic;
         s : out std_logic_vector(32 downto 0));
	end component;
      
	-- Clock period definitions
   constant clk_period : time := 10 ns;
   
	-- Inputs
   signal a_s : std_logic_vector(31 downto 0) := (others => '0');
   signal b_s : std_logic_vector(31 downto 0) := (others => '0');
   signal clk_s : std_logic := '0';

 	-- Outputs
   signal s_s : std_logic_vector(32 downto 0);
	
	-- File
	file vectors : text;
	file expected: text;
	
begin 
	
	-- Instantiate the Unit Under Test (UUT)
   uut: top_level_add port map (
          a => a_s,
          b => b_s,
          clk => clk_s,
          s => s_s
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk_s <= '0';
		wait for clk_period/2;
		clk_s <= '1';
		wait for clk_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
		variable v_iline : line;
		variable a_txt: std_logic_vector(31 downto 0);
		variable b_txt: std_logic_vector(31 downto 0);
   begin
		file_open(vectors, "vectors.txt",  read_mode);
		while not endfile(vectors) loop
			readline(vectors, v_iline);
			hread(v_iline, a_txt);
			hread(v_iline, b_txt);
			a_s <= a_txt;
			b_s <= b_txt;
			wait for clk_period;
		end loop;
		file_close(vectors);
      wait;
   end process;
	
	-- Assert process
	assr_proc: process
		variable w_iline : line;
		variable s_txt: std_logic_vector(35 downto 0);
		variable i: integer := 0;
	begin
		wait for clk_period;												-- needed for core latency
		file_open(expected, "expected.txt", read_mode);
		while not endfile(expected) loop
			wait for clk_period;
			i := i + 1;														-- used for rapid debug of vector file
			readline(expected, w_iline);
			hread(w_iline, s_txt);
			assert s_s = s_txt(32 downto 0) 
			report "Assertion violation at row" & integer'image(i);
		end loop;
		file_close(expected);
		wait;
	end process;
end;
