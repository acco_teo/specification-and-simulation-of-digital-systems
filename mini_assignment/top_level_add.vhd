----------------------------------------------------------------------------------
-- Company:
-- Engineer:			Matteo Accornero
-- 
-- Create Date:		 
-- Design Name: 
-- Module Name:		top_level_add - behavioral 
-- Project Name: 		Mini-Assignment
-- Target Devices: 
-- Tool versions: 
-- Description:		top level wrapper for 32 bits signed adder
--							generated with Xilinx IP
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top_level_add is
	port(	a : in std_logic_vector(31 downto 0);
			b : in std_logic_vector(31 downto 0);
			clk : in std_logic;
			s : out std_logic_vector(32 downto 0));
end top_level_add;

architecture behavioral of top_level_add is
	
	component add_core
		port( 
		a : in std_logic_vector(31 downto 0);
		b : in std_logic_vector(31 downto 0);
		clk : in std_logic;
		s : out std_logic_vector(32 downto 0));
	end component;

begin
	
	core_instance : add_core
	port map(a => a,
				b => b,
				clk => clk,
				s => s);

end behavioral;

