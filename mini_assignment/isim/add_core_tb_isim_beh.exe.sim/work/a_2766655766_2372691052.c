/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Matteo/Documents/Specification and Simulation of Digital Systems/MiniAssignment/m_a/add_core_tb.vhd";
extern char *STD_TEXTIO;
extern char *IEEE_P_3564397177;
extern char *STD_STANDARD;

void ieee_p_3564397177_sub_3988856810_91900896(char *, char *, char *, char *, char *);


static void work_a_2766655766_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 3624U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 4504);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(69, ng0);
    t2 = (t0 + 1808U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3432);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 4504);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(71, ng0);
    t2 = (t0 + 1808U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3432);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_2766655766_2372691052_p_1(char *t0)
{
    char t5[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    int64 t13;

LAB0:    t1 = (t0 + 3872U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(80, ng0);
    t2 = (t0 + 2616U);
    t3 = (t0 + 8732);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 1;
    t7 = (t6 + 4U);
    *((int *)t7) = 11;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (11 - 1);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    std_textio_file_open1(t2, t3, t5, (unsigned char)0);
    xsi_set_current_line(81, ng0);

LAB4:    t2 = (t0 + 2616U);
    t10 = std_textio_endfile(t2);
    t11 = (!(t10));
    if (t11 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(89, ng0);
    t2 = (t0 + 2616U);
    std_textio_file_close(t2);
    xsi_set_current_line(90, ng0);

LAB14:    *((char **)t1) = &&LAB15;

LAB1:    return;
LAB5:    xsi_set_current_line(82, ng0);
    t3 = (t0 + 3680);
    t4 = (t0 + 2616U);
    t6 = (t0 + 2896U);
    std_textio_readline(STD_TEXTIO, t3, t4, t6);
    xsi_set_current_line(83, ng0);
    t2 = (t0 + 3680);
    t3 = (t0 + 2896U);
    t4 = (t0 + 1928U);
    t6 = *((char **)t4);
    t4 = (t0 + 8316U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(84, ng0);
    t2 = (t0 + 3680);
    t3 = (t0 + 2896U);
    t4 = (t0 + 2048U);
    t6 = *((char **)t4);
    t4 = (t0 + 8332U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t6, t4);
    xsi_set_current_line(85, ng0);
    t2 = (t0 + 1928U);
    t3 = *((char **)t2);
    t2 = (t0 + 4568);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t12 = *((char **)t7);
    memcpy(t12, t3, 32U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(86, ng0);
    t2 = (t0 + 2048U);
    t3 = *((char **)t2);
    t2 = (t0 + 4632);
    t4 = (t2 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t12 = *((char **)t7);
    memcpy(t12, t3, 32U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(87, ng0);
    t2 = (t0 + 1808U);
    t3 = *((char **)t2);
    t13 = *((int64 *)t3);
    t2 = (t0 + 3680);
    xsi_process_wait(t2, t13);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB6:;
LAB8:    goto LAB4;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

LAB12:    goto LAB2;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

}

static void work_a_2766655766_2372691052_p_2(char *t0)
{
    char t6[16];
    char t24[16];
    char t26[16];
    char *t1;
    char *t2;
    char *t3;
    int64 t4;
    char *t5;
    char *t7;
    char *t8;
    int t9;
    unsigned int t10;
    unsigned char t11;
    unsigned char t12;
    int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    char *t17;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t25;
    char *t27;
    char *t28;
    unsigned int t29;
    unsigned int t30;

LAB0:    t1 = (t0 + 4120U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(99, ng0);
    t2 = (t0 + 1808U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 3928);
    xsi_process_wait(t2, t4);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(100, ng0);
    t2 = (t0 + 2720U);
    t3 = (t0 + 8743);
    t7 = (t6 + 0U);
    t8 = (t7 + 0U);
    *((int *)t8) = 1;
    t8 = (t7 + 4U);
    *((int *)t8) = 12;
    t8 = (t7 + 8U);
    *((int *)t8) = 1;
    t9 = (12 - 1);
    t10 = (t9 * 1);
    t10 = (t10 + 1);
    t8 = (t7 + 12U);
    *((unsigned int *)t8) = t10;
    std_textio_file_open1(t2, t3, t6, (unsigned char)0);
    xsi_set_current_line(101, ng0);

LAB8:    t2 = (t0 + 2720U);
    t11 = std_textio_endfile(t2);
    t12 = (!(t11));
    if (t12 != 0)
        goto LAB9;

LAB11:    xsi_set_current_line(109, ng0);
    t2 = (t0 + 2720U);
    std_textio_file_close(t2);
    xsi_set_current_line(110, ng0);

LAB26:    *((char **)t1) = &&LAB27;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(102, ng0);
    t3 = (t0 + 1808U);
    t5 = *((char **)t3);
    t4 = *((int64 *)t5);
    t3 = (t0 + 3928);
    xsi_process_wait(t3, t4);

LAB14:    *((char **)t1) = &&LAB15;
    goto LAB1;

LAB10:;
LAB12:    xsi_set_current_line(103, ng0);
    t2 = (t0 + 2288U);
    t3 = *((char **)t2);
    t9 = *((int *)t3);
    t13 = (t9 + 1);
    t2 = (t0 + 2288U);
    t5 = *((char **)t2);
    t2 = (t5 + 0);
    *((int *)t2) = t13;
    xsi_set_current_line(104, ng0);
    t2 = (t0 + 3928);
    t3 = (t0 + 2720U);
    t5 = (t0 + 2968U);
    std_textio_readline(STD_TEXTIO, t2, t3, t5);
    xsi_set_current_line(105, ng0);
    t2 = (t0 + 3928);
    t3 = (t0 + 2968U);
    t5 = (t0 + 2168U);
    t7 = *((char **)t5);
    t5 = (t0 + 8348U);
    ieee_p_3564397177_sub_3988856810_91900896(IEEE_P_3564397177, t2, t3, t7, t5);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 1512U);
    t3 = *((char **)t2);
    t2 = (t0 + 2168U);
    t5 = *((char **)t2);
    t10 = (35 - 32);
    t14 = (t10 * 1U);
    t15 = (0 + t14);
    t2 = (t5 + t15);
    t11 = 1;
    if (33U == 33U)
        goto LAB18;

LAB19:    t11 = 0;

LAB20:    if (t11 == 0)
        goto LAB16;

LAB17:    goto LAB8;

LAB13:    goto LAB12;

LAB15:    goto LAB13;

LAB16:    t17 = (t0 + 8755);
    t19 = ((STD_STANDARD) + 384);
    t20 = (t0 + 2288U);
    t21 = *((char **)t20);
    t9 = *((int *)t21);
    t20 = xsi_int_to_mem(t9);
    t22 = xsi_string_variable_get_image(t6, t19, t20);
    t25 = ((STD_STANDARD) + 1008);
    t27 = (t26 + 0U);
    t28 = (t27 + 0U);
    *((int *)t28) = 1;
    t28 = (t27 + 4U);
    *((int *)t28) = 26;
    t28 = (t27 + 8U);
    *((int *)t28) = 1;
    t13 = (26 - 1);
    t29 = (t13 * 1);
    t29 = (t29 + 1);
    t28 = (t27 + 12U);
    *((unsigned int *)t28) = t29;
    t23 = xsi_base_array_concat(t23, t24, t25, (char)97, t17, t26, (char)97, t22, t6, (char)101);
    t28 = (t6 + 12U);
    t29 = *((unsigned int *)t28);
    t30 = (26U + t29);
    xsi_report(t23, t30, 2);
    goto LAB17;

LAB18:    t16 = 0;

LAB21:    if (t16 < 33U)
        goto LAB22;
    else
        goto LAB20;

LAB22:    t7 = (t3 + t16);
    t8 = (t2 + t16);
    if (*((unsigned char *)t7) != *((unsigned char *)t8))
        goto LAB19;

LAB23:    t16 = (t16 + 1);
    goto LAB21;

LAB24:    goto LAB2;

LAB25:    goto LAB24;

LAB27:    goto LAB25;

}


extern void work_a_2766655766_2372691052_init()
{
	static char *pe[] = {(void *)work_a_2766655766_2372691052_p_0,(void *)work_a_2766655766_2372691052_p_1,(void *)work_a_2766655766_2372691052_p_2};
	xsi_register_didat("work_a_2766655766_2372691052", "isim/add_core_tb_isim_beh.exe.sim/work/a_2766655766_2372691052.didat");
	xsi_register_executes(pe);
}
